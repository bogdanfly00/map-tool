'use strict';

angular.module('myApp.main', ['ngRoute','ngMap'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/', {
    templateUrl: 'main/main.html',
    controller: 'MainCtrl'
  });
}])

.controller('MainCtrl', function( $scope, $http,/* $sce,*/ $location, $log, NgMap) {

	//$log.info( 'query string=',$location.search().file );
	var file ="test_data/locations.json";
	if( $location.search().file ){
		file = "out/"+$location.search().file;
		$scope.outFile = file;
	}
	
	$http.get('../services/'+file).then(function successCallback(response){
		//$scope.thisCanBeusedInsideNgBindHtml = $sce.trustAsHtml(response.data);
		//$scope.mapLocations = response.data;
		$scope.markers= response.data;
		
	});
	
	var vm = this;
	NgMap.getMap().then(function(map){
		//console.log(map.getCenter());
		//console.log('markers', map.markers);
		//console.log('shapes', map.shapes);
		vm.map = map;
	});
	vm.template = {
		cached:'info-window-template.html'
	};
	
	vm.showInfo = function(evt, markId, markAdd){
		$scope.markAdd=markAdd;
		vm.map.showInfoWindow('cached', this);
	};
	
});