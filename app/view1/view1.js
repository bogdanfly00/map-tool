'use strict';

angular.module('myApp.view1', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/view1', {
    templateUrl: 'view1/view1.html',
    controller: 'View1Ctrl'
  });
}])

.controller('View1Ctrl', function( $scope, $http, $window ) {

	// everything for the map

	// locations
	var promise = $http.get('locations.json');
	
	$window.locations = "";
	promise.then(function(response) {
		$window.locations = response.data;
			// sets all the markers
			var marker, i;
			for (i = 0; i < $window.locations.length; i++) {
				marker = new google.maps.Marker({
					position: new google.maps.LatLng(locations[i][1], locations[i][2]),
					map: map,
					icon: 'images/marker.png'
				});
		
				google.maps.event.addListener(marker, 'click', (function(marker, i) {
					return function() {
						infowindow.setContent(locations[i][0]);
						infowindow.open(map, marker);
					}
				})(marker, i))
			}
		
		}, function(reason) {
		console.log('Failed to load json data!: ' + reason);
	});



    // creates a new map and sets the first location
	$window.map = new google.maps.Map(document.getElementById('map'), {
		center: {
			lat: 39.5,
			lng: -98.35
		},
		zoom: 3,
		mapTypeId: google.maps.MapTypeId.ROADMAP
	})

	$window.infowindow = new google.maps.InfoWindow();




	// SEARCH BAR
	// Create the search box and link it to the UI element.
	var input = document.getElementById('pac-input');
	var searchBox = new google.maps.places.SearchBox(input);
	// $window.map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

	// Bias the SearchBox results towards current map's viewport.
	$window.map.addListener('bounds_changed', function() {
	  searchBox.setBounds(map.getBounds());
	});

	$window.markers = [];
	// Listen for the event fired when the user selects a prediction and retrieve
	// more details for that place.
	searchBox.addListener('places_changed', function() {
	  var places = searchBox.getPlaces();

	  if (places.length == 0) {
	    return;
	  }

	  // For each place, get the icon, name and location.
	  var bounds = new google.maps.LatLngBounds();
	  places.forEach(function(place) {
	    var icon = {
	      url: place.icon,
	      size: new google.maps.Size(71, 71),
	      origin: new google.maps.Point(0, 0),
	      anchor: new google.maps.Point(17, 34),
	      scaledSize: new google.maps.Size(25, 25)
	    };

	    if (place.geometry.viewport) {
	      // Only geocodes have viewport.
	      bounds.union(place.geometry.viewport);
	    } else {
	      bounds.extend(place.geometry.location);
	    }
	  });
	  $window.map.fitBounds(bounds);
	});

	$('#googleSubmit').on('click', function() {
		var searchResult = document.getElementById('pac-input');

		google.maps.event.trigger(searchResult, 'focus')
		google.maps.event.trigger(searchResult, 'keydown', {
			keyCode: 13
		})
	})
});

