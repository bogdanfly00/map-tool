<?php
/**
 * IO handles all input output file operations
 *
 * @author Bogdan Bodnarescu
*/

require_once 'Map.php';

class IO {
    
  public $inDir = "in";
  
  public $inFilename = "";
  
  public $outDir = "out";
  
  public $outFilename = "";
  
  public function __construct(){
      
  }

  /**
   * IO Upload file
   *
   * @param file $_FILES['file']
  */
  public function upload(){
    #$filename = $this->_genFileName();
      
    if(!$_FILES)
      #die('unknown error');
      throw new Exception('unknown error');
        
    $file = array_values($_FILES)[0];
    if ( $file['error']>0)
      #die('file error!');
      throw new Exception( 'file error = '. $this->_dump($file) );
    
    if (!file_exists($this->inDir)) {
      mkdir($this->inDir, 0777, true);
      #echo 'create IN dir: '.$this->inDir;
    }
    
    $filename = $this->_genFileName( $this->inDir, $file['name'] );
        
    if ( move_uploaded_file( $file['tmp_name'] , $this->inDir."\\" . $filename ) === TRUE){
      #$this->inFilename = $file['name'];
      $this->inFilename = $filename;
      return TRUE;
    }
    
    return FALSE;
  }

  public function _genFileName( $dir, $filename){
    $rand = tempnam ( $dir, '' );
    
    $tmp =  substr(strrchr($rand, "\\"), 1);
    $ext = pathinfo($dir."\\".$filename, PATHINFO_EXTENSION);
    $new_file = str_replace( "tmp", $ext, $tmp);
    return $new_file;
  }
  
  /**
   * @return $addresses to json
   */
  public function export_geocode_to_json( $content ){
    #$handle = @fopen($this->addressesCSV, "w");
     $filename = $this->inFilename.".json";
     
        if (!file_exists($this->outDir)) {
          mkdir($this->outDir, 0777, true);
          #echo 'create OUT dir: '.$this->outDir;
        }
        if (!$handle = fopen( $this->outDir. "/". $filename, 'w')) {
             #echo "Cannot open file ($filename)";
             exit;
        }
    
        if (fwrite($handle, $content) === FALSE) {
            #echo "Cannot write to file ($filename)";
            exit;
        }
        #echo "Success ! we write to  the file ($filename)
        #<br>
        #content: <br>
        #$content";
        fclose($handle);
        $this->outFilename = $filename;
    
  }
  
  public function _dump($data){
    if (func_num_args() > 1) {
       $data = func_get_args();
    }
    return "<pre>".print_r($data, 1)."</pre>";
  }
    

}
 
 // incrase execution time !!
set_time_limit ( 90 );
#phpinfo();
$io = new IO();
#var_dump($_FILES);
try{
    $io->upload();
}catch(Exception $e){
    #echo $io->_dump( $e );
}

//test genFileName
#var_dump($io->_genFileName( 'in', 'test.txt' ));

//get uploaded filename
#var_dump($io);

$map = new Map();
$map->addressesCSV = $io->inDir . "/" . $io->inFilename;


$content = $map->geocode_batch();

#var_dump($map, $content);

$io->export_geocode_to_json( $content );
#var_dump($io);
header('Location:'.$_SERVER['HTTP_REFERER']."#!/?file=$io->outFilename&error=$map->errors");
