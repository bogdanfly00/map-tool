<?php

#namespace \Test;

use PHPUnit\Framework\TestCase;

#class StupidTest extends \PHPUnit_Framework_TestCase
class StupidTest extends TestCase
{
  
  public function testTrueIsTrue()
  {
    $foo = true;
    $this->assertTrue($foo);
  }
  
 
  public function testPushAndPop()
  {
    $stack = [];
    $this->assertEquals(0, count($stack));
    
    array_push($stack, 'foo');
    $this->assertEquals('foo', $stack[count($stack)-1]);
    $this->assertEquals(1, count($stack));
    
    $this->assertEquals('foo', array_pop($stack));
    $this->assertEquals(0, count($stack));
  }
  
  
  public function testEmpty(){
    $stack = [];
    $this->assertEmpty($stack);
    
    return $stack;
  }
  
  /**
   *  @depends testEmpty
   */
  public function testPush( array $stack ){
    array_push($stack, 'foo');
    $this->assertEquals('foo', $stack[count($stack)-1]);
    $this->assertNotEmpty($stack);
    
    return $stack;
  }
  
  /**
   * @depends testPush
   */
  public function testPop( array $stack ){
    $this->assertEquals('foo', array_pop($stack));
    $this->assertEmpty($stack);
  }
  
  public function testOne(){
    $this->assertTrue(false);
  }
  
  public function testProducerFirst(){
    $this->assertTrue(true);
    return 'first';
  }
  
  public function testProducerSecond(){
    $this->assertTrue(true);
    return 'second';
  }
  
  /**
   * @depends testProducerFirst
   * @depends testProducerSecond
   */
  public function testConsumer(){
    $this->assertEquals(['first', 'second'],
                        func_get_args());
  }
  
  /**
   * @dataProvider additionProvider
   */
  public function testAdd($a, $b, $expected){
    $this->assertEquals( $expected, $a + $b);
  }
  
  public function additionProvider(){
    return [
      [0,0,0],
      [0,1,1],
      'one plus zero' => [1,0,1],
      'one plus one'  => [1,1,3]
    ];
  }
  
  
  
}