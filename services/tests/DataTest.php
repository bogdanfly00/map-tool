<?php

#namespace php\Map;

use PHPUnit\Framework\TestCase;

#set_include_path(get_include_path(). PATH_SEPARATOR. 'src' . PATH_SEPARATOR . 'test_data');
set_include_path('test_data');
#var_dump(get_include_path());

require'CsvFileIterator.php';

class DataTest extends TestCase
{
  /**
   * @dataProvider additionProvider
   */
  public function testAdd($a, $b, $expected){
    $this->assertEquals($expected, $a + $b);
  }
  
  public function additionProvider(){
    return new CsvFileIterator("data.csv");
  }
}
