<?php
// http://stackoverflow.com/questions/13767135/email-function-using-templates-includes-via-ob-start-and-global-vars
class SimpleTemplate {
	private $_tpl = "";
  private $_vars = array();
  
  function __construct($tpl_name){
    $this->_tpl = $tpl_name;
  }
  
  public function __set($name, $value){
    $this->_vars[$name] = $value;
  }
  
  public function setVars($values){
    $this->_vars = $values;
  }
  
  public function parse(){
    ob_start();
    extract($this->_vars);
    include $this->_tpl;
    return ob_get_clean();
  }
}

?>
