<?php

require_once('SimpleTemplate.php');

class MapRender extends Map{
	
  #public function __construct(){
  #  parent::__construct();
  #}
  
  /**
   * Print Map JS in header.
   */
  public function mapJS(){
    // creates a new map and sets the first location
    ob_start() ?>
    <script>
    $window.map = new google.maps.Map(document.getElementById('map'), {
      <?php echo $this->map_params(); ?>
    });
    </script>
    <?php
    $ret = ob_get_contents();
    ob_end_clean();
    return $ret;
  }
  
  /**
   * Print map as script in body.
   */
  public function showMap(){
    $tpl = new SimpleTemplate('map.tpl.php');
    $tpl->map = $this;
    echo $tpl->parse();
  }
 
}
