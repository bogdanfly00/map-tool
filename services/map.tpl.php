<!DOCTYPE html>
<html>

<head>
	<style>
		#map {
			height: 400px;
			width: 100%;
		}
	</style>
	<script src="https://code.jquery.com/jquery-3.0.0.min.js" integrity="sha256-JmvOoLtYsmqlsWxa7mDSLMwa6dZ9rrIdtrrVYRnDRH0=" crossorigin="anonymous"></script>

</head>

<body>
	<h3>My Google Maps Demo</h3>
	<div id="map"></div>
	<script>
		function initMap() {
			map = new google.maps.Map(document.getElementById('map'), <?= $map->map_params() ?>);

			var $window = window;
			var infowindow = new google.maps.InfoWindow();
			$.getJSON('../services/test_data/locations.json')
				.done(function(data) {
					$window.locations = data;

					// sets all the markers
					var marker, i;
					for (i = 0; i < $window.locations.length; i++) {
						marker = new google.maps.Marker({
							position: new google.maps.LatLng(locations[i][1], locations[i][2]),
							map: map
						});
            google.maps.event.addListener(marker, 'click', (function(marker, i) {
              return function() {
                infowindow.setContent(locations[i][0]);
                infowindow.open(map, marker);
              };
            })(marker, i));
          }
				});
		}
	</script>
	<script async defer src="https://maps.googleapis.com/maps/api/js?key=<?= $map->apikey() ?>&callback=initMap">
	</script>
</body>

</html>