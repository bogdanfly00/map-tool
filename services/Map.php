<?php
/**
 * Google Map class
 *
 * @author Bogdan Bodnarescu
 * 
 */

#require 'SimpleTemplate.php';

class Map {
  
  private $_id = "";
  
  public $center = array( 'lat'=>39.5, 'lng'=> -98.35 );
  
  public $scrollWheel = FALSE;
  
  public $zoom = 3;
  
  #public $mapTypeId = "google.maps.MapTypeId.ROADMAP";
  
  public $errors = array();
  
  //AIzaSyBFlgYJg5J6v7xTNmQJ7LPgacVeaM_zlls -> GlenGrant - working keys 
  //AIzaSyBLVpkD9Xl8c-ja7pRmpOx_JLfuE-4ob_4  -> GlenGrant-dev on alert@flycommunications.com
  public $_key = "AIzaSyBFlgYJg5J6v7xTNmQJ7LPgacVeaM_zlls";
  
  public $_url = "https://maps.googleapis.com/maps/api/js?key=AIzaSyBFlgYJg5J6v7xTNmQJ7LPgacVeaM_zlls&libraries=places";
  
  public $_geocode_url = "https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyBFlgYJg5J6v7xTNmQJ7LPgacVeaM_zlls&address=";
  
  public $addressesCSV = "map_raw_lite.csv";
  
  /**
   * @param $_geocodes[] full addresses returned by Geocode API
   */
  private $_geocodes = array();
  
  #public function __construct(){
    #$this->result = file_get_contents( $this->_geocode_url.$this->address );
    #$this->_id = uniqid("map");
    #return $this;
  #}
  
  public function apikey(){
    return $this->_key;
  }
  
  /**
   * @return $addresses[] from a CSV file @addressesCSV
   */
  public function import_addresses_from_csv(){
    $handle = @fopen($this->addressesCSV, "r");
    $addresses = array();
    if ($handle) {
    
      while (($buffer = fgetcsv($handle, 4096)) !== false) {
          $addresses[] = $buffer;
      }
      if (!feof($handle)) {
        echo "Error: unexpected fgets() fail\n";
      }
      fclose($handle);
    }
    return $addresses;
  }
  
  /**
   * @
   * @return JSON gGeocode + raw reqeust
   */
  private function _geocode_request( $address ){
   //filter place name !!!
   $tmp = $address;
   unset($tmp[0]);
   $address_req = urlencode(implode(',', $tmp));
   $ret[] = file_get_contents( $this->_geocode_url.$address_req );
   $ret['request'] = $address;
   return $ret;
  }
  
  /**
   * @param $addresses[] || import_addresses_from_csv
   * @return JSON ARRAY
   */
  public function geocode_batch( $addresses = array() ){
    $data = array();
    if( empty($addresses) ){
      $addresses = $this->import_addresses_from_csv();
    }
    
    foreach( $addresses as $address ){
      $data[] = $this->_geocode_request($address);
    }
    
    $this->_geocodes = $data ;
    #var_dump( $data );
    return $this->_filter_locations();
  }
  

  
  //to do: format sub brand, place name, address, city, state
  /**
   * @todo:
   * output geocode_batch as json array , ready to be imported into JS
   * format:
   * place name, coordinate ( lat, long )
   * @return $ret JSON
   */
  public function _filter_locations(){
    $_result =  $this->_geocodes;
    $ret = array();
    $err = array();
    
  
    
    if ($_result){
      foreach( $_result as $r){
        $ret = json_decode($r[0], true);
        if (!empty($ret)){
          $req = $r['request'];
          $store_name = $req[0];
          $return[] = array(
                          implode(",",[$store_name, $ret['results'][0]['formatted_address']]),
                          #$ret['results'][0]['formatted_address'],
                          $ret["results"][0]['geometry']['location']['lat'],
                          $ret["results"][0]['geometry']['location']['lng'] );
          
        }
        else
          $err[] = array("error",urldecode( implode( ",",$r["request"])) );
      }
      
      #$this->_dump($return);
      #die('die' );
      
      $this->errors = json_encode($err);
      
      return json_encode($return);
    }
  }
      
      
      function _dump($data) {
				if (func_num_args() > 1) {
					$data = func_get_args();
				}
				echo "<pre>".print_r($data, 1)."</pre>";
			}
  
  /**
   * map params for JS
   * @return MAP as JSON
   */
  public function map_params(){
    #unset( $this->map );
    return json_encode( $this );
    
  }
  
  /**
   * Encode array into Json
   */
  private function json($data){
    if(is_array($data)){
      return json_encode($data);
    }
  }

  
}